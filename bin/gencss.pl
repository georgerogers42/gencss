#!/usr/bin/env perl
use strict;
use warnings;
use autodie;
use JSON::XS qw(decode_json);
use FindBin;
use Try::Tiny;
use lib "$FindBin::Bin/../lib";
use GenCSS::Gen;

my $ofname = shift;
open my $of, ">", $ofname;
try {
  my $g = GenCSS::Gen->new($of);
  for my $fname (@ARGV) {
    $g->from_filename($fname);
  }
} catch {
  die $_;
} finally {
  close $of;
};
