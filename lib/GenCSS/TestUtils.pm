package GenCSS::TestUtils;
use base qw(Exporter);
use strict;
use warnings;
use autodie;
use Test::More;
use Try::Tiny;
use JSON::XS qw(decode_json);
use GenCSS::Gen;

our @EXPORT_OK = qw(test_file);

sub test_file {
  my ($fname) = @_;
  my $output = "";
  open my $w, ">", \$output;
  my $gen = GenCSS::Gen->new($w);
  $gen->from_filename("data/$fname.json");
  open my $cf, "<", "data/$fname.css";
  my $cd = try {
    join("", <$cf>);
  } finally {
    close $cf;
  };
  ok($output eq $cd);
}

1;
