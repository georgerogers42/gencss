package GenCSS::Gen;
use base qw(GenCSS::Base);
use 5.20.0;
use strict;
use warnings;
use autodie;
use Try::Tiny;
use JSON::XS qw(decode_json);

=head1 GenCSS::Gen
A class to generate CSS from lispy data.

=head2 Methods
=cut


__PACKAGE__->mk_accessors(qw(w));

=over
=cut
=item init

Initializes object for new.
Takes in the output file.
=cut

sub init {
  my ($self, $w) = @_;
  $self->SUPER::init;
  $self->w($w);
}

sub output_head {
  my ($self, $head) = @_;
  $self->w->printf("%s {\n", join(", ", @$head));
}

sub output_body {
  my ($self, $body) = @_;
  for my $p (@$body) {
    my ($k, @v) = @$p;
    if(ref $k eq 'ARRAY') {
      $self->output_rule($k);
    } else {
      $self->w->printf("%s: %s;\n", $k, join(", ", @v));
    }
  }
  $self->w->say("}");
}

sub output_rule {
  my ($self, $rule) = @_;
  if($rule->[0] eq '@') {
    if(@$rule > 3) {
      $self->w->printf("@%s %s {\n", $rule->[1], join(", ", @{$rule->[2]}));
      $self->output_body([@$rule[3..@$rule-1]]);
    } else {
      $self->w->printf("@%s %s;\n", $rule->[1], join(", ", @{$rule->[2]}));
    }
  } elsif(ref $rule->[0]) {
    $self->output_head($rule->[0]);
    $self->output_body([@$rule[1..@$rule-1]]);
  }
}

sub output_rules {
  my ($self, $rules) = @_;
  for my $rule (@$rules) {
    $self->output_rule($rule);
  }
}

=item output

Outputs a CSS file based on data passed in.

For Example:

  my $output = "";
  my $data = [];
  open my $of, ">", \$output;
  my $gen = GenCSS::Gen->new($of);
  $gen->output($data);

=cut
sub output {
  my ($self, @args) = @_;
  $self->output_rules(@args);
}

sub from_filename {
  my ($self, $fname) = @_;
  open my $f, "<", $fname;
  my $data = try {
    decode_json(join("", <$f>));
  } catch {
    die $_;
  } finally {
    close $f;
  };
  $self->output($data);
}
=back
=cut

1;
