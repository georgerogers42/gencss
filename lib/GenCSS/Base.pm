package GenCSS::Base;
use base qw(Class::Accessor);
use 5.20.0;
use strict;
use warnings;
use threads::shared;

sub new {
  my ($class, @args) = @_;
  my $self = bless {}, $class;
  $self->init(@args);
  return $self;
}

sub init {
}

1;
